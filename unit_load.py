# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.

from trytond.pool import PoolMeta, Pool
from itertools import groupby

__all__ = ['UnitLoad']


class UnitLoad(metaclass=PoolMeta):
    __name__ = 'stock.unit_load'

    @classmethod
    def _assing_ul_moves(cls, records):
        pool = Pool()
        Move = pool.get('stock.move')
        UnitLoad = pool.get('stock.unit_load')

        uls = [r for r in records if r.production_state != 'running']
        if uls:
            super()._assing_ul_moves(uls)

        running_uls = list(set(records) - set(uls))

        input_moves = [m for r in running_uls for m in r.moves
            if m.state == 'draft' and m.from_location.type == 'storage']
        output_moves = [m for r in running_uls for m in r.moves
            if m.state == 'draft' and m.from_location.type == 'production']

        if input_moves:
            Move.assign_try(input_moves)
            running_uls = UnitLoad.browse(map(int, running_uls))
            input_moves = [m for r in running_uls for m in r.moves
                if m.to_location.type == 'production']
            to_assign = []
            for m in input_moves:
                if m.state == 'draft':
                    if m.product.lot_is_required(m.from_location,
                            m.to_location):
                        if m.lot:
                            to_assign.append(m)
                    else:
                        to_assign.append(m)
            if to_assign:
                Move.assign(to_assign)

            running_uls = UnitLoad.browse(map(int, running_uls))

            input_moves = [m for r in running_uls for m in r.moves
                if m.state == 'assigned' and
                    m.to_location.type == 'production']
            input_moves = [m for m in input_moves if m.lot]
            input_moves = sorted(input_moves,
                key=lambda x: (x.unit_load, x.product))
            input_moves = {key_: list(recs)
                for key_, recs in groupby(input_moves,
                    key=lambda x: (x.unit_load, x.product))}
            for m in output_moves[:]:
                if m.lot:
                    continue
                key_ = (m.unit_load, m.product)
                if key_ in input_moves:
                    imoves = input_moves[key_]
                    imoves_qty = sum(im.quantity for im in imoves)
                    if m.quantity > imoves_qty:
                        output_moves.extend(Move.copy([m], {
                            'quantity': m.quantity - imoves_qty}))
                    m.lot = imoves[0].lot
                    m.quantity = imoves[0].quantity
                    m.save()
                    for im in imoves[1:]:
                        output_moves.extend(Move.copy([m], {
                            'lot': im.lot.id,
                            'quantity': im.quantity}))

        if output_moves:
            Move.assign(output_moves)

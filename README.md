datalife_stock_unit_load_auto_lot
=================================

The stock_unit_load_auto_lot module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-stock_unit_load_auto_lot/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-stock_unit_load_auto_lot)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT

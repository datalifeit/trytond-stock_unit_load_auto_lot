=================================
Stock Unit Load Auto Lot Scenario
=================================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()
    >>> yesterday = today - relativedelta(days=1)


Install stock_unit_load_auto_lot::

    >>> config = activate_modules(['stock_unit_load_auto_lot',
    ...     'stock_lot_fifo',])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create product::

    >>> Template = Model.get('product.template')
    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> goods_template = Template()
    >>> goods_template.name = 'Goods product'
    >>> goods_template.default_uom = unit
    >>> goods_template.type = 'goods'
    >>> goods_template.list_price = Decimal('5')
    >>> goods_template.save()
    >>> goods_product, = goods_template.products
    >>> goods_product.cost_price = Decimal('3')

    >>> aux_template = Template()
    >>> aux_template.name = 'Aux. product'
    >>> aux_template.default_uom = unit
    >>> aux_template.type = 'goods'
    >>> aux_template.list_price = Decimal('5')
    >>> aux_template.lot_force_assign = True
    >>> lot_required = ['storage']
    >>> aux_template.lot_required = lot_required
    >>> aux_template.save()
    >>> aux_product, = aux_template.products
    >>> aux_product.cost_price = Decimal('3')
    >>> aux_product.save()

Configure product lot required:

    >>> ltypes = ['supplier', 'customer', 'lost_found', 'storage', 'production']
    >>> aux_template.lot_required = ltypes
    >>> aux_template.save()


Create lots::

    >>> Lot = Model.get('stock.lot')
    >>> lot1 = Lot(number='Lot 1', product=aux_product)
    >>> lot1.save()
    >>> lot2 = Lot(number='Lot 2', product=aux_product)
    >>> lot2.save()


Create supplier::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()


Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> supplier_loc, = Location.find([('code', '=', 'SUP')])
    >>> input_loc, = Location.find([('code', '=', 'IN')])
    >>> production_loc, = Location.find([('type', '=', 'production')])
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])


Create Shipment In::

    >>> ShipmentIn = Model.get('stock.shipment.in')
    >>> shipment_in = ShipmentIn()
    >>> shipment_in.supplier = supplier
    >>> shipment_in.warehouse = warehouse_loc
    >>> shipment_in.company = company
    >>> shipment_in.effective_date = today
    >>> shipment_in.planned_date = today
    >>> move = shipment_in.incoming_moves.new()
    >>> move.product = aux_product
    >>> move.uom = unit
    >>> move.quantity = 2
    >>> move.from_location = supplier_loc
    >>> move.to_location = input_loc
    >>> move.lot = lot1
    >>> move.unit_price = Decimal('3')
    >>> move = shipment_in.incoming_moves.new()
    >>> move.product = aux_product
    >>> move.uom = unit
    >>> move.quantity = 3
    >>> move.from_location = supplier_loc
    >>> move.to_location = input_loc
    >>> move.lot = lot2
    >>> move.unit_price = Decimal('3')
    >>> shipment_in.click('receive')
    >>> shipment_in.click('done')


Create Shipment In with yesterday date::

    >>> shipment_in2 = ShipmentIn()
    >>> shipment_in2.supplier = supplier
    >>> shipment_in2.warehouse = warehouse_loc
    >>> shipment_in2.company = company
    >>> shipment_in2.effective_date = yesterday
    >>> shipment_in2.planned_date = yesterday
    >>> move = shipment_in2.incoming_moves.new()
    >>> move.product = aux_product
    >>> move.uom = unit
    >>> move.quantity = 2
    >>> move.from_location = supplier_loc
    >>> move.to_location = input_loc
    >>> move.lot = lot1
    >>> move.unit_price = Decimal('3')
    >>> shipment_in2.click('receive')
    >>> shipment_in2.click('done')


Create unit load with yesterday date::

    >>> UnitLoad = Model.get('stock.unit_load')
    >>> unit_load = UnitLoad()
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = production_loc
    >>> unit_load.product = goods_product
    >>> unit_load.cases_quantity = 5
    >>> unit_load.quantity = Decimal('35.0')
    >>> unit_load.start_date = datetime.datetime.now() - relativedelta(days=1)
    >>> unit_load.end_date = datetime.datetime.now() - relativedelta(days=1)
    >>> for m in unit_load.production_moves:
    ...     m.from_location = production_loc
    ...     m.to_location = storage_loc
    >>> input_move = unit_load.production_moves.new()
    >>> input_move.product = aux_product
    >>> input_move.quantity = Decimal('5')
    >>> input_move.from_location = storage_loc
    >>> input_move.to_location = production_loc
    >>> input_move.unit_price = Decimal('3')
    >>> output_move = unit_load.production_moves.new()
    >>> output_move.product = aux_product
    >>> output_move.quantity = Decimal('5')
    >>> output_move.from_location = production_loc
    >>> output_move.to_location = storage_loc
    >>> output_move.unit_price = Decimal('3')
    >>> unit_load.click('assign')
    >>> unit_load.click('do')
    >>> len([m for m in unit_load.production_moves if m.lot == lot1 and m.quantity == 2])
    2
    >>> len([m for m in unit_load.production_moves if m.lot == lot1 and m.quantity > 2])
    0
    >>> len([m for m in unit_load.production_moves if m.lot == lot2])
    0
    >>> len([m for m in unit_load.production_moves if not m.lot and m.quantity == 3])
    2


Create an unit load::

    >>> unit_load = UnitLoad()
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = production_loc
    >>> unit_load.product = goods_product
    >>> unit_load.cases_quantity = 5
    >>> unit_load.quantity = Decimal('35.0')
    >>> for m in unit_load.production_moves:  
    ...     m.from_location = production_loc
    ...     m.to_location = storage_loc
    >>> input_move = unit_load.production_moves.new()
    >>> input_move.product = aux_product
    >>> input_move.quantity = Decimal('5')
    >>> input_move.from_location = storage_loc
    >>> input_move.to_location = production_loc
    >>> input_move.unit_price = Decimal('3')
    >>> output_move = unit_load.production_moves.new()
    >>> output_move.product = aux_product
    >>> output_move.quantity = Decimal('5')
    >>> output_move.from_location = production_loc
    >>> output_move.to_location = storage_loc
    >>> output_move.unit_price = Decimal('3')
    >>> unit_load.click('assign')
    >>> unit_load.click('do')
    >>> len([m for m in unit_load.production_moves if m.lot == lot1 and m.quantity == 4])
    2
    >>> len([m for m in unit_load.production_moves if m.lot == lot2 and m.quantity == 1])
    2
    >>> len([m for m in unit_load.production_moves if not m.lot])
    1

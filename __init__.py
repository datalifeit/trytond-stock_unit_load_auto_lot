# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import unit_load
from . import move


def register():
    Pool.register(
        unit_load.UnitLoad,
        move.Move,
        module='stock_unit_load_auto_lot', type_='model')
